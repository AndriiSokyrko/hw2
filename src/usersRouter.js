const express = require('express');

const usersRouter = express.Router();
const {
  changeProfilePassword,
  deleteProfile,
  getProfileInfo,
} = require('./usersService.js');
const { authMiddleware } = require('./middleware/authMiddleware.js');

usersRouter.get('/', authMiddleware, getProfileInfo);

usersRouter.patch('/', authMiddleware, changeProfilePassword);

usersRouter.delete('/', authMiddleware, deleteProfile);

module.exports = {
  usersRouter
};
