const { Note } = require('./models/Notes');

async function addUserNotes(req, res) {
  try {
    const { userId } = req.user;
    const { text } = req.body;

    if (typeof text === 'undefined' || text.trim === '') {
      return res.status(400).json({ message: 'Text is empty' });
    }
    const createdDate = new Date().toISOString()
    const note = new Note({
      text,
      userId,
      createdDate
    });
    return await note.save().then((saved) => {
      res.status(200).json(saved);
    });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
}

const getNoteById = async (req, res) => {
  try {
    return await Note.findById({
      _id: req.params.id,
    })
      .then((note) => {
        if (!note || typeof note === 'null') {
          return res.status(400)
            .json({ message: "Note isn't found" });
        }
        return res.status(200)
          .json(note._doc);
      });
  } catch (e) {
    return res.status(500)
      .json({ message: 'Server error' });
  }
};

const deleteUserNoteById = async (req, res) => {
  try {
    if (typeof req.params.id === 'undefined' || req.params.id.trim === '') {
      return res.status(400).json({ message: 'Id is empty' });
    }
    const note = await Note.findById(
        req.params.id)
    const noteUserId = JSON.stringify(note.userId).slice(1,-1)

    if(noteUserId!==req.user.userId){
      return res.status(400).json({ message: "You aren't the owner of note" });
    }
    return Note.findByIdAndDelete({
      _id: req.params.id,
      userId: req.user.userId,
    }).then((note) => {
      if (!note || typeof note === 'null') {
        return res.status(400).json({ message: "Note isn't exist" });
      }
      return res.status(200).json({ message: 'Success', ...note._doc });
    });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const getUserNotes = async (req, res) => {
  try {
     const notes = await Note.find({userId:req.user.userId})
    if(notes===null){
      return res.status(200).json('No notes');
    }
       return res.status(200).json(notes);

  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const updateUserNoteById = async (req, res, next) => {
  try {
    const { text } = req.body;
    if (typeof req.params.id === 'undefined' || req.params.id.trim === '') {
      return res.status(400).json({ message: 'Id is empty' });
    }
    if (typeof text.trim === '') {
      return res.status(400).json({ message: 'Text is empty' });
    }
    const note = await Note.findById(
      req.params.id)
    const noteUserId = JSON.stringify(note.userId).slice(1,-1)
    if(noteUserId!==req.user.userId){
      return res.status(400).json({ message: "You aren't the owner of note" });
    }
    return await Note.findByIdAndUpdate({
          _id: req.params.id,
          userId: req.user.userId },
        { $set: { text } }, { new: true })
      .then((note) => {
        if (!note || typeof note === 'null') {
          return next(
            res.status(400)
              .json({ message: "Note isn't found" }),
          );
        }
        return res.status(200).json({ message: 'Success', ...note._doc });
      });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const toggleCompletedForUserNoteById = async (req, res, next) => {
  try {
    if (typeof req.params.id === 'undefined' || req.params.id.trim === '') {
      return res.status(400).json({ message: 'Id is empty' });
    }
    await Note.findById({
      _id: req.params.id,
    })
      .then((note) => {
        Note.findByIdAndUpdate({ _id: req.params.id }, { $set: { checked: !note.checked } }, { new: true })
          .then((result) => {
            if (!result) {
              return res.status(400)
                .json({ message: "Note isn't found" });
            }
            return res.status(200).json({ message: 'Success', ...result._doc });
          });
      });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

module.exports = {
  addUserNotes,
  getUserNotes,
  getNoteById,
  deleteUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
};
