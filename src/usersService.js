const bcrypt = require('bcryptjs');
// const jwt = require('jsonwebtoken');
require('dotenv').config()

const { User } = require('./models/Users');

const changeProfilePassword = async (req, res) => {
    try {
        if (typeof req.body.password === 'undefined' || req.body.password.trim === '') {
            return res.status(400)
                .json({ message: 'Password is empty' });
        }
        const { password } = req.body;
        const cryptPassword = bcrypt.hashSync(password, 10);
        await User.findByIdAndUpdate(
            { _id: req.user.userId },
            { $set: { password: cryptPassword } },
        )
            .then((user) => {
                if (!user) {
                    return res.status(400)
                        .json({ message: "User isn't found" });
                }

                const payload = {
                    userId: user._id,
                    username: user.username,
                };

                const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
                return res.status(200).json({
                    message: 'Success',
                    jwt_token: jwtToken,
                })

            });
    } catch (e) {
        return res.status(500)
            .json({ message: 'Server error' });
    }
};


const deleteProfile = (req, res) => {
  try {
    return User.findByIdAndDelete({
      _id: req.user.userId,
    })
      .then((user) => {
        if (!user) {
          return res.status(400)
            .json({ message: "User isn't found" });
        }
        return res.status(200)
          .json({ message: 'success', ...user });
      });

  } catch (e) {
    return res.status(500)
      .json({ message: 'Server error' });
  }
};

const getProfileInfo = (req, res) => {
  try {
    return User.findById({
      _id: req.user.userId,
    })
      .then((user) => {
        if (!user || typeof user ==='null') {
          return res.status(400)
            .json({ message: "User isn't found" });
        }
        return res.status(200)
          .json(user._doc);
      });
  } catch (e) {
    return res.status(500)
      .json({ message: 'Server error' });
  }
};
module.exports = {
  changeProfilePassword,
  deleteProfile,
  getProfileInfo,
};
