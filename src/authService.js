const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/Users');
require('dotenv').config()

const createProfile = async (req, res, next) => {

  try {
    const { username, password } = req.body;
    if (typeof username === 'undefined') {
      return res.status(400).json({ message: 'Username is empty' });
    }
    const checkUserName = await User.findOne({ username });

    if (checkUserName) {
      return res.status(400).json({ message: 'Username is exist' });
    }


    const cryptPassword =  bcrypt.hashSync(password, 10);
    const user = new User({
      username,
      password: cryptPassword,
    });
     user.save()
      .then((saved) => {
         return res.status(200).json({ message: 'Success', ...saved._doc });
      })
      .catch((err) => {
        next(err);
      });
  } catch (e) {
    res.status(500).json({ message: 'Server   error' });
  }
};

const login = async (req, res, next) => {
  try {
    const user = await User.findOne({ username: req.body.username });
    if(user){
    bcrypt.compare(String(req.body.password), String(user.password) ,
        (err, isMatched)=>{
            if(!isMatched){
                return res.status(400)
                    .json({message: 'Not authorized password'});
            }
          const payload = {
              userId: user._id,
              username: user.username,
          };

          const jwtToken = jwt.sign(payload,  process.env.SECRET_KEY);
          return res.status(200).json({
            message: 'Success',
            jwt_token: jwtToken,
          })
        })

    } else {
      return res.status(403)
          .json({message: 'User not found'});
    }
  } catch (e) {
    res.status(500).json({ message: 'Server error' });
  }
};
module.exports = {
  createProfile,
  login,

};
