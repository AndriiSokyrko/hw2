const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  checked: {
    type: Boolean,
    default: false,
  },
  createdDate: {
    type: String,
    required: true
  }
});

const Note = mongoose.model('note', noteSchema);

module.exports = {
  Note,
};
