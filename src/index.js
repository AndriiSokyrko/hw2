require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');

const app = express();

const {authRouter} = require('./authRouter.js');
const {notesRouter} = require('./notesRouter.js');
const {usersRouter} = require('./usersRouter.js');

const port = process.env.PORT || 8080;
app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  try {
    await mongoose.connect(process.env.URL_MONGO);
    app.listen(port);
    console.log(`Server listening on port ${port}`);
  } catch (err) {
    console.log(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  res.status(500).send({'message': 'Server error'});
}
