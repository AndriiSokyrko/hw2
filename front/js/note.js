class Note {

    constructor(
        path = 'http://127.0.0.1:8080/api/notes',
        auth_key = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImVyZG9nYW4iLCJ1c2VySWQiOiI2MmY3NmI0YjFmMGQ3ZTM2YWU0Yzk3ZWUiLCJpYXQiOjE2NjAzODgwOTB9.TJ9OhO9WOP7Jbli8Vjej_0UrxmAVtHZDj-2qt1QIono')
    {
        this.loading = false
        this.error = null
        this.path = path
        this.auth_key = auth_key
    }

    setLoading(status) {
        this.loading = status
    }

    setError(error) {
        this.error = error
    }

    async  setQuery(url , method = 'get', body = null, headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImVyZG9nYW4iLCJ1c2VySWQiOiI2MmY3NmI0YjFmMGQ3ZTM2YWU0Yzk3ZWUiLCJpYXQiOjE2NjAzODgwOTB9.TJ9OhO9WOP7Jbli8Vjej_0UrxmAVtHZDj-2qt1QIono'
    }) {
        this.setLoading(true);
        try {
            let options;
            if(!body){
                options = {
                    method: method,
                    headers: headers,
                    body: body
                }
            } else {
                options =  {
                    method: method,
                    headers: headers
                };
            }
            const response = await fetch(url,options );
            if (!response.ok) {
                this.setError('MY error'+data.message);
                throw new Error(data.message || "Error conection to data")
            }
            const data = response.json();

            this.setLoading(false);
            return data;
        } catch (e) {
            this.setLoading(false);
            this.setError(e.message);
            throw e;
        }
    }
    getAllNotes() {
          this.setQuery(this.path ).then(data=> {
              console.log(data)
          return data
          })
    }

    getNoteById(id) {
        return this.query(this.path + `/${id}`)

    }

    checkNoteById(id) {
        return this.query(this.path + `/${id}`, 'patch')
    }

    checkNoteById(id) {
        return this.query(this.path + `/${id}`, 'delete')
    }

    modiNoteById(id, payload) {
        return this.query(this.path + `/${id}`, 'put', payload)
    }
}

module.exports = {
    Note
}
